minivimrc
=========

This **minivimrc** started its life as a very small `vimrc` I put together while working on a friend's dedicated server. Since then, it slowly became a sophisticated but compact `vimrc` that — with only 60-something LOC — provides quite a lot of useful improvements to Vim's defaults **without** depending on any third-party or perverting my favorite editor's philosophy and design.

**Don't use it if you don't understand what's in there!**

## Features

* filetype detection and syntax highlighting
* a minimal set of useful defaults
* automatic opening of the quickfix/location window
* easy spaces ↔︎ tabs manual switching
* mappings for juggling with files
* mappings for juggling with buffers
* mappings for juggling with definitions (definition-search, tags)
* mappings for juggling with matches (include-search)
* mappings for smoothing out search and replace
* a more useful :Grep command
* mappings for juggling with quickfix entries
* mappings for a better command-line
* mappings for smoothing out the use of list-like commands
* mappings for a more intuitive completion menu
* simplistic brace expansion
* small colorscheme tweaks
